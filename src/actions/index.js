export const selectSearch = search => {
  console.log(
    'You clicked on search: ID: ' +
      search.id +
      ' Nome: ' +
      search.nome +
      ' Idade: ' +
      search.idade
  );
  return {
    type: 'SEARCH_SELECTED',
    payload: search
  };
};

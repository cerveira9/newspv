import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { selectSearch } from '../actions/index';

class SearchList extends Component {
  createListItems() {
    return this.props.search.map(search => {
      return (
        <li key={search.id}
            onClick={() => this.props.selectSearch(search)}
        >
          {search.nome} {search.idade}
        </li>
      );
    });
  }

  render() {
    return (
      <div>
        <ul>{this.createListItems()}</ul>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    search: state.search
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ selectSearch }, dispatch);
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(SearchList);

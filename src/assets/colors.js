export const PURPLE_LIGHT = '#302D43';
export const PURPLE_DARK = '#282639';
export const GREEN_NEON = '#92CF48';
export const GREY_LIGHT = '#F4F6F9';
export const GREY_DARK = '#EAEDF3';

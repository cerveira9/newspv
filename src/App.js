import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faStroopwafel,
  faHome,
  faStickyNote,
  faRibbon,
  faAddressBook,
  faCog,
  faSearch,
  faFileSignature,
  faUser,
  faAt
} from '@fortawesome/free-solid-svg-icons';
import Sidebar from './components/Sidebar';
import Header from './components/Header';
import ContentArea from './components/ContentArea';
import CadastroArea from './components/CadastroArea';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

library.add(
  faStroopwafel,
  faHome,
  faStickyNote,
  faRibbon,
  faAddressBook,
  faCog,
  faSearch,
  faFileSignature,
  faUser,
  faAt
);

class App extends Component {
  render() {
    return (
      <div>
        <Sidebar />        
        <BrowserRouter>
          <div>
            <Route exact path="/" component={ContentArea} />
            <Route exact path="/Cadastro" component={CadastroArea} />
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;

const styles = {
  container: {
    display: 'flex',
    flexDisplay: 'column'
  }
};

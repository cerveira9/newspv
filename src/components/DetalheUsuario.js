import React, { Component } from 'react';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import EditarUsuario from './EditarUsuario';

export default class DetalheUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,

      id: props.id,
      nome: props.nome,
      login: props.login,
      facebook: props.facebook,
      instagram: props.instagram,
      email: props.email,

      editar: false
    };
  }

  onClickEditar() {
    this.setState({ editar: true });
  }

  onClickVoltarEditar() {
    this.setState({ editar: false });
  }

  renderCard() {
    return (
      <div>
        <Card
          title={this.props.nome}
          subTitle={this.props.login}
          footer={
            <span>
              <Button
                label="Editar"
                style={{ marginRight: '1em' }}
                className="p-button-rounded p-button-info"
                onClick={this.onClickEditar.bind(this)}
              />
              <Button
                label="Voltar"
                className="p-button-raised p-button-rounded p-button-secondary"
                onClick={this.props.voltaLista}
              />
            </span>
          }
        >
          <p>ID: {this.props.id}</p>
          <p>Facebook: {this.props.facebook}</p>
          <p>Instagram: {this.props.instagram}</p>
          <p>Email: {this.props.email}</p>
        </Card>
      </div>
    );
  }

  render() {
    if (this.state.editar) {
      return (
        <div>
          <EditarUsuario
            voltarEditar={this.onClickVoltarEditar.bind(this)}
            id={this.state.id}
            nome={this.state.nome}
            login={this.state.login}
            facebook={this.state.facebook}
            instagram={this.state.instagram}
            email={this.state.email}
          />
        </div>
      );
    } else {
      return <div>{this.renderCard()}</div>;
    }
  }
}

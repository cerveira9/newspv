import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './CadastroArea.css';
import CadastroUsuario from './CadastroUsuario';
import CadastroDica from './CadastroDica';
import CadastroPromocao from './CadastroPromocao';

export default class CadastroArea extends Component {
  render() {
    return (
      <div id="container_content">
        <div className="heading">
          <h1>Cadastro</h1>
          <p>Área de Cadastro de: usuário, dica ou promoção.</p>
        </div>

        <div className="cards">
          <div className="card col-md-4">
            <h6>Cadastrar Usuário</h6>
            <CadastroUsuario />
          </div>

          <div className="card col-md-4">
            <h6>Cadastrar Dica</h6>
            <CadastroDica />
          </div>

          <div className="card_promocao col-md-4">
            <h6>Cadastrar Promoção</h6>
            <CadastroPromocao />
          </div>
        </div>
      </div>
    );
  }
}

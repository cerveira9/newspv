import React, { Component } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Card } from 'primereact/card';
import './ListaUsuarios.css';

export default class ListaDicas extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dicas: [],

      loading: true
    };
  }

  componentWillMount() {
    this.getListaUsuarios();
  }

  getListaUsuarios() {
    axios
      .get('http://191.252.200.224:8080/rest/webservice/getListaDicas/0')
      .then(resposta => {
        this.setState({ dicas: resposta.data, loading: false });
      })
      .catch();
  }

  renderCards() {
    return this.state.dicas.map(dica => (
      <div>
        <Card title={dica.titulo} subTitle={dica.resumo} header={<img alt="imgDica" src={dica.imagem} />} />
      </div>
    ));
  }

  render() {
    return <div>{this.renderCards()}</div>;
  }
}

import React, { Component } from 'react';
import axios from 'axios';
import { Button } from 'primereact/button';
import { ProgressSpinner } from 'primereact/progressspinner';
import { InputText } from 'primereact/inputtext';

export default class EditarUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loadingDeletar: false,

      id: props.id,
      nome: props.nome,
      login: props.login,
      facebook: props.facebook,
      instagram: props.instagram,
      email: props.email
    };
  }

  editarRequest(event) {
    event.preventDefault();

    this.setState({ loading: true });

    axios
      .post('http://191.252.200.224:8080/rest/webservice/cadastrarUsuario', {
        id: this.state.id,
        nome: this.state.nome,
        login: this.state.login,
        facebook: this.state.facebook,
        instagram: this.state.instagram,
        email: this.state.email
      })
      .then(resposta => {
        alert('Usuário Cadastrado com Sucesso!');
        this.setState({ loading: false });
      })
      .catch(erro => {
        alert('Algo deu errado, tente novamente!');
        this.setState({ loading: false });
      });
  }

  deletarUsuario() {
    this.setState({ loadingDeletar: true });
    axios
      .post('http://191.252.200.224:8080/rest/webservice/removerUsuario', {
        id: this.state.id
      })
      .then(() => {
        alert('Usuário Deletado!');
        this.setState({ loadingDeletar: false });
      })
      .catch(erro => {
        alert('Algo de errado aconteceu. ERRO: ' + erro);
        this.setState({ loadingDeletar: false });
      });
  }

  renderButton() {
    if (!this.state.loading) {
      return (
        <div>
          <Button
            className="p-button-rounded p-button-info p-button-raised "
            label="Cadastrar"
            type="submit"
            style={{ marginRight: '1em' }}
          />
          <Button
            className="p-button-raised p-button-rounded p-button-secondary"
            label="Voltar"
            onClick={this.props.voltarEditar}
          />
          <br />
        </div>
      );
    } else {
      return <ProgressSpinner />;
    }
  }

  renderButtonDeletar() {
    if (!this.state.loadingDeletar) {
      return (
        <Button
          className="p-button-raised p-button-rounded p-button-danger"
          label="Deletar Usuário"
          style={{ marginTop: '1em' }}
          onClick={this.deletarUsuario.bind(this)}
        />
      );
    } else {
      return <ProgressSpinner />;
    }
  }

  render() {
    return (
      <div>
        <form id="form-style" onSubmit={this.editarRequest.bind(this)}>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-drivers-license" style={{ color: 'purple' }} />
            </span>
            <InputText
              placeholder="Nome"
              name="nome"
              onChange={event => this.setState({ nome: event.target.value })}
            />
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-user" style={{ color: 'purple' }} />
            </span>
            <InputText
              placeholder="Login"
              name="login"
              onChange={event => this.setState({ login: event.target.value })}
            />
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-facebook" style={{ color: 'purple' }} />
            </span>
            <InputText
              placeholder="Facebook"
              name="facebook"
              onChange={event =>
                this.setState({ facebook: event.target.value })
              }
            />
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-instagram" style={{ color: 'purple' }} />
            </span>
            <InputText
              placeholder="Instagram"
              name="instagram"
              onChange={event =>
                this.setState({ instagram: event.target.value })
              }
            />
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-at" style={{ color: 'purple' }} />
            </span>
            <InputText
              placeholder="Email"
              name="email"
              onChange={event => this.setState({ email: event.target.value })}
            />
          </div>
          <div id="button-usuario">{this.renderButton()}</div>
        </form>
        {this.renderButtonDeletar()}
      </div>
    );
  }
}

import React, { Component } from 'react';
import axios from 'axios';
import DetalheUsuario from './DetalheUsuario';
import { Card } from 'primereact/card';

export default class ManageUsuario extends Component {
    constructor(props) {
        super(props);
    
        console.log(props.lista);
    
        this.state = {
          usuarios: [],
    
          loading: true,

          detalhes: false,
        };
      }
    
      componentWillMount() {
          if (this.state.usuarios.length == 0) this.getListaUsuarios();
      }
    
      getListaUsuarios() {
        axios
          .get('http://191.252.200.224:8080/rest/webservice/getListaUsuarios/0')
          .then(resposta => {
            this.setState({ usuarios: resposta.data, loading: false });
          })
          .catch();
      }
    
      renderCards() {
        return this.state.usuarios.map(usuario => (
          <div onClick={this.setState({ detalhes: true })}>
            <Card title={usuario.nome} subTitle={usuario.login} />
          </div>
        ));
      }

      renderDetalhes() {
        if (!this.state.detalhes) {
            return (
                this.renderCards()
            );
        } else {
            <DetalheUsuario />
        }
      }
    
      render() {
        return <div>{this.renderCards()}</div>;
      }
}

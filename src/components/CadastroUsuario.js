import React, { Component } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { ProgressSpinner } from 'primereact/progressspinner';
import './CadastroUsuario.css';

export default class CadastroUsuario extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      login: '',
      facebook: '',
      instagram: '',
      email: '',

      loading: false,

    };

  }

  cadastrarUsuario(event) {
    event.preventDefault();

    this.setState({ loading: true });

    axios
      .post('http://191.252.200.224:8080/rest/webservice/cadastrarUsuario', {
        nome: this.state.nome,
        login: this.state.login,
        facebook: this.state.facebook,
        instagram: this.state.instagram,
        email: this.state.email
      })
      .then(resposta => {
        alert('Usuário Cadastrado com Sucesso!');
        this.setState({ loading: false });
      })
      .catch(erro => {
        alert('Algo deu errado, tente novamente!');
        this.setState({ loading: false });
      });
  }



  renderButton() {
    if (!this.state.loading) {
      return (
        <Button
          className="p-button-raised p-button-rounded p-button-secondary"
          label="Cadastrar"
          type="submit"
        />
      );
    } else {
      return <ProgressSpinner />;
    }
  }

  renderForm() {
    return (
      <div>
        <form id="form-style" onSubmit={this.cadastrarUsuario.bind(this)}>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-drivers-license" style={{ color: 'purple' }} />
            </span>
            <InputText
              placeholder="Nome"
              name="nome"
              onChange={event => this.setState({ nome: event.target.value })}
            />
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-user" style={{ color: 'purple' }} />
            </span>
            <InputText placeholder="Login" name="login"
              onChange={event => this.setState({ login: event.target.value })}/>
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-facebook" style={{ color: 'purple' }} />
            </span>
            <InputText placeholder="Facebook" name="facebook"
              onChange={event => this.setState({ facebook: event.target.value })}/>
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-instagram" style={{ color: 'purple' }} />
            </span>
            <InputText placeholder="Instagram" name="instagram"
              onChange={event => this.setState({ instagram: event.target.value })}/>
          </div>
          <div className="p-inputgroup">
            <span className="p-inputgroup-addon">
              <i class="fa fa-at" style={{ color: 'purple' }} />
            </span>
            <InputText placeholder="Email" name="email"
              onChange={event => this.setState({ email: event.target.value })}/>
          </div>
          <div id="button-usuario">{this.renderButton()}</div>
        </form>
      </div>
    );
  }

  render() {
    return <div>{this.renderForm()}</div>;
  }
}

import React, { Component } from 'react';
import axios from 'axios';
import FileBase64 from 'react-file-base64';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { ProgressSpinner } from 'primereact/progressspinner';
import { InputTextarea } from 'primereact/inputtextarea';
import './CadastroPromocao.css';

export default class CadastroPromocao extends Component {
  constructor(props) {
    super(props);

    this.state = {
      imagem: '',
      facebook: '',

      loading: false
    };
    this.state = {
      files: []
    };
  }

  getFiles(files) {
    this.setState({ files: files });
    this.state.files.map(file => {
      this.setState({ imagem: file.base64 });
    });
  }

  cadastrarUsuario(event) {
    event.preventDefault();

    this.setState({ loading: true });

    axios
      .post('http://191.252.200.224:8080/rest/webservice/cadastrarPromocao', {
        imagem: this.state.imagem,
        facebook: this.state.facebook
      })
      .then(resposta => {
        alert('Promoção Cadastrada com Sucesso!');
        this.setState({ loading: false });
      })
      .catch(erro => {
        alert('Algo deu errado, tente novamente!');
        this.setState({ loading: false });
      });
  }

  renderButton() {
    if (!this.state.loading) {
      return (
        <Button
          className="p-button-raised p-button-rounded p-button-secondary"
          label="Cadastrar"
          type="submit"
        />
      );
    } else {
      return <ProgressSpinner />;
    }
  }

  renderForm() {
    return (
      <div>
        <form onSubmit={this.cadastrarUsuario.bind(this)}>
          <div id="form-style">
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <i class="fa fa-upload" style={{ color: 'purple' }} />
              </span>
              <FileBase64 multiple={true} onDone={this.getFiles.bind(this)} />
            </div>
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <i class="fa fa-facebook" style={{ color: 'purple' }} />
              </span>
              <InputText
                placeholder="Título"
                name="facebook"
                onChange={event =>
                  this.setState({ facebook: event.target.value })
                }
              />
            </div>
          </div>
          <div id="button-usuario">{this.renderButton()}</div>
        </form>
      </div>
    );
  }

  render() {
    return <div>{this.renderForm()}</div>;
  }
}

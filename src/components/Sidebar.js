import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Sidebar.css';

export default class Sidebar extends Component {
  render() {
    return (
      <div>
        <section id="sideMenu">
          <div id="navigation">
            <div className="logo-img" />
            <a href="/">
              <FontAwesomeIcon className="fa" icon="home" />
              Dashboard
            </a>
            <a href="/Cadastro">
              <FontAwesomeIcon className="fa" icon="address-book" />
              Cadastro
            </a>
            <a href="#">
              <FontAwesomeIcon className="fa" icon="cog" />
              Sobre
            </a>
          </div>
        </section>
      </div>
    );
  }
}

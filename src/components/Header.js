import React, { Component } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Header.css';

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
      busca: []
    };
  }

  searchBar() {
    axios
      .get(
        'http://191.252.200.224:8080/rest/webservice/buscarUsuario/' +
          this.state.search
      )
      .then(resposta => {
        this.props.buscar(resposta.data);
        //this.setState({ busca: resposta.data });
      });
  }

  render() {
    return (
      <div id="container">
        <div className="search_area">
          <FontAwesomeIcon className="fa_search" icon="search" />
          <input
            className="input"
            type="text"
            name="search"
            onChange={event => this.setState({ search: event.target.value })}
          />
        </div>
        <div className="user_area">
          <a href="#" onClick={this.searchBar.bind(this)} className="a">
            Buscar
          </a>
        </div>
      </div>
    );
  }
}

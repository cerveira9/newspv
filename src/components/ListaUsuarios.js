import React, { Component } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import DetalheUsuario from './DetalheUsuario';
import { Card } from 'primereact/card';
import './ListaUsuarios.css';

export default class ListaUsuarios extends Component {
  constructor(props) {
    super(props);

    console.log(props.lista);

    this.state = {
      usuarios: props.lista,

      loading: true,

      detalhes: false,

      id: null,
      nome: null,
      login: null,
      facebook: null,
      instagram: null,
      email: null
    };
  }

  componentWillMount() {
    if (this.state.usuarios.length == 0) this.getListaUsuarios();
  }

  getListaUsuarios() {
    axios
      .get('http://191.252.200.224:8080/rest/webservice/getListaUsuarios/0')
      .then(resposta => {
        console.log(resposta);
        this.setState({ usuarios: resposta.data, loading: false });
      })
      .catch();
  }

  onClickCard(
    idUsuario,
    nomeUsuario,
    loginUsuario,
    facebookUsuario,
    instagramUsuario,
    emailUsuario
  ) {
    this.setState({
      detalhes: true,
      id: idUsuario,
      nome: nomeUsuario,
      login: loginUsuario,
      facebook: facebookUsuario,
      instagram: instagramUsuario,
      email: emailUsuario
    });
  }

  renderCards() {
    return this.state.usuarios.map(usuario => (
      <div
        onClick={this.onClickCard.bind(
          this,
          usuario.id,
          usuario.nome,
          usuario.login,
          usuario.facebook,
          usuario.instagram,
          usuario.email
        )}
      >
        <Card id={usuario.id} title={usuario.nome} subTitle={usuario.login} />
      </div>
    ));
  }

  renderListaChild(){
    this.setState({ detalhes: false })
    this.getListaUsuarios()
  }

  render() {
    if (this.state.detalhes) {
      return (
        <div>
          <DetalheUsuario
            voltaLista={this.renderListaChild.bind(this)}
            id={this.state.id} 
            nome={this.state.nome} 
            login={this.state.login} 
            facebook={this.state.facebook} 
            instagram={this.state.instagram} 
            email={this.state.email}/>
        </div>
      );
    } else {
      return <div>{this.renderCards()}</div>;
    }
  }
}

import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ListaUsuarios from './ListaUsuarios';
import ListaDicas from './ListaDicas';
import Header from './Header';
import ListaPromocoes from './ListaPromocoes';
import SearchList from '../containers/searchList';
import './ContentArea.css';
import ManageUsuario from './ManageUsuario';


export default class ContentArea extends Component {

  constructor(props) {
      super(props);
      this.state = {
          usuarios: []
      };
  }

  comportamento(usuarios) {
      this.setState({
          usuarios
      });
  }

  render() {
    return (
      <div id="container_content">
        <Header buscar={this.comportamento.bind(this)} />

        <div className="heading">
          <h1>Dashboard</h1>
          <p>Bem Vindo ao SPV!</p>
        </div>

        <div className="cards">
          <div className="card col-md-4">
            <h6>Usuários</h6>
            <ListaUsuarios lista={this.state.usuarios} />
            {/* <ManageUsuario lista={this.state.usuarios}/> */}
          </div>

          <div className="card col-md-4">
            <h6>Dicas</h6>
            <ListaDicas />
          </div>

          <div className="card_promocao col-md-4">
            <h6>Promoções</h6>
            <ListaPromocoes />
          </div>
        </div>
      </div>
    );
  }
}

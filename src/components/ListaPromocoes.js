import React, { Component } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Card } from 'primereact/card';
import './ListaPromocoes.css';

export default class ListaPromocoes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      promocoes: [],

      loading: true
    };
  }

  componentWillMount() {
    this.getListaUsuarios();
  }

  getListaUsuarios() {
    axios
      .get('http://191.252.200.224:8080/rest/webservice/getListaPromocoes/5')
      .then(resposta => {
        this.setState({ promocoes: resposta.data, loading: false });
      })
      .catch();
  }

  renderCards() {
    return this.state.promocoes.map(promocao => (
      <div className="row">
        <div id="promocoes-card">
          <Card
            header={
              <img
                alt="imgPromocao"
                src={promocao.imagem}
                style={{ width: '100%' }}
              />
            }
          >
            <a href={promocao.facebook}>Link</a>
          </Card>
        </div>
      </div>
    ));
  }

  render() {
    return <div>{this.renderCards()}</div>;
  }
}

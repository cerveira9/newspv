import React, { Component } from 'react';
import '/Volumes/HDD/Code/SPECTRAL/NEW SPV/new-spv/src/tests/ideias.css';

export default class Ideias extends Component {
  render() {
    return (
      <div>
        <div className="title-box">
          <h1 className="title-style">SPV - Sistema Pós Venda</h1>
        </div>
        <div id="box-container-out">
          <div id="box-container">
            <div id="box1">
              <i
                class="material-icons"
                style={{
                  fontSize: 80,
                  marginLeft: 60,
                  marginTop: 55,
                  color: '#005e9d'
                }}
              >
                store
              </i>
              <p
                style={{
                  textAlign: 'center',
                  fontWeight: 900,
                  color: '#56b5f5'
                }}
              >
                LOJAS
              </p>
            </div>
            <div id="box2">
              <i
                class="fa fa-user"
                style={{
                  fontSize: 80,
                  marginLeft: 70,
                  marginTop: 55,
                  color: '#05987f'
                }}
              />
              <p
                style={{
                  textAlign: 'center',
                  fontWeight: 900,
                  color: '#54ecd3'
                }}
              >
                USUÁRIOS
              </p>
            </div>
            <div id="box3">
              <i
                class="fa fa-file-text"
                style={{
                  fontSize: 80,
                  marginLeft: 70,
                  marginTop: 55,
                  color: '#c15f07'
                }}
              />
              <p
                style={{
                  textAlign: 'center',
                  fontWeight: 900,
                  color: '#f4a259'
                }}
              >
                RELATÓRIOS
              </p>
            </div>
            <div id="box4">
              <i
                class="fa fa-cog"
                style={{
                  fontSize: 80,
                  marginLeft: 70,
                  marginTop: 55,
                  color: '#9b3341'
                }}
              />
              <p
                style={{
                  textAlign: 'center',
                  fontWeight: 900,
                  color: '#ff7789'
                }}
              >
                CONFIGURAÇÕES
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

import {combineReducers} from 'redux';
import reducerSearch from './reducerSearch';
import activeSearchReducer from './reducerActiveSearch';

const allReducers = combineReducers({
    search: reducerSearch,
    activeSearch: activeSearchReducer
})

export default allReducers;